#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

import qrcode
import pandas as pd

import tkinter as tk
import tkinter.filedialog as fd
import pandas as pd
import os
import random
import shutil
import time
import datetime

from fpdf import FPDF
from PIL import Image
import os

class App(tk.Tk):

    def __init__(self):
        super(App, self).__init__()
        self.var = tk.StringVar()
        self.initUI()
        # os.makedirs('orimg', exist_ok=True)
        self.orimg_path = 'orimg/'
        self.bgc_path ='pics/'
        self.TTF = 'ttc/msyh.ttc'

    def initUI(self):
        self.title("青岛工贸职业学校资产管理系统")
        group_top = tk.LabelFrame(self, padx=15, pady=10)
        group_top.pack(padx=10, pady=5)
        self.title=tk.Label(group_top, text='青岛工贸职业学校二维码软件', font=('宋体',14), fg='green').grid(row=0, column=0, columnspan=2)
        tk.Button(group_top, text="选择文件", width=10, command=self.choose_file).grid(row=1, column=1)

        self.path_entry = tk.Entry(group_top, width=30)
        self.path_entry.grid(row=1, column=0, sticky=tk.W)

        self.savepath=tk.StringVar()
        e3=tk.Entry(group_top, width=30, textvariable=self.savepath).grid(row=3, column=0, sticky=tk.W)
        bFile=tk.Button(group_top, width=12, text='结果保存到',command=self.saveFile).grid(row=3, column=1)
        # l=tk.Label(group_top, height=1).grid(row=3, column=0)
        tk.Button(group_top, text="开始执行", width=10, command=self.mainPro).grid(row=5, column=0, pady=10,sticky=tk.W)
        tk.Button(group_top, text="停止", width=10, command=self.destroy).grid(row=5, column=1, pady=10,sticky=tk.E)
        # self.feet=tk.Label(group_top, text='Powered by 青岛引领信息科技有限公司', font=('宋体',10), fg='blue').grid(row=7, column=0, columnspan=2, pady=0,sticky=tk.S)

        console_frame = tk.Frame(group_top).grid(row=2, column=0, columnspan=2)
        self.console_text = tk.Text(
            console_frame, fg="green", bg="black", width=40, height=20, state=tk.DISABLED)
        scrollbar = tk.Scrollbar(console_frame, command=self.console_text.yview)
        scrollbar.pack(side="right", fill=tk.Y)
        self.console_text.pack(expand=1, fill=tk.BOTH)
        self.console_text['yscrollcommand'] = scrollbar.set


    def output_to_console(self, new_text):
        self.console_text.config(state=tk.NORMAL)
        self.console_text.insert(tk.END, new_text)
        self.console_text.see(tk.END)
        # self.console_text.config(state=tk.DISABLED)

    def choose_file(self):
        filetypes = (("Excel", "*.csv"),
                     ("All files", "*"))
        self.filename = fd.askopenfilename(title="Open file", initialdir="/",
                                      filetypes=filetypes)
        if self.filename:
            self.path_entry.delete("0", tk.END)
            self.path_entry.insert(tk.END, self.filename)
            self.output_to_console(f"{self.filename}\n")

    def saveFile(self):
        # global outPath
        self.outPath = fd.askdirectory(title="Save Path", initialdir="/")
        self.outFile = self.outPath+'/'
        self.savepath.set(self.outFile)

        if self.savepath:
            self.output_to_console(f"{self.outFile}\n")

    def func(self):
        content = self.content
        self.output_to_console(f"{content}\n")

    # def QRcode(self):

    def make_qr(self, data, file):
        qr=qrcode.QRCode(
            version=5,
            error_correction=qrcode.constants.ERROR_CORRECT_H,
            box_size=15,
            border=6,
        )
        qr.add_data(data)
        qr.make(fit=True)
        img = qr.make_image()
        img = img.convert("RGBA")

        icon = Image.open(self.bgc_path + "logo.jpg")

        img_w, img_h = img.size
        factor = 4
        size_w = int(img_w / factor)
        size_h = int(img_h / factor)

        icon_w, icon_h = icon.size
        if icon_w > size_w:
            icon_w = size_w
        if icon_h > size_h:
            icon_h = size_h
        icon = icon.resize((icon_w, icon_h), Image.ANTIALIAS)

        w = int((img_w - icon_w) /2)
        h = int((img_h - icon_h) /2)
        icon = icon.convert("RGBA")
        self.w = img_w
        self.h = img_h
        img.paste(icon, (w,h), icon)
        os.makedirs('orimg', exist_ok=True)
        filename = 'orimg/ori_'+file
        img.save(filename)

    def codeTitle(self, QR_IMG, content, name, cla, file):

        img = Image.open(QR_IMG)
        draw = ImageDraw.Draw(img)
        font_title = ImageFont.truetype('fonts\\simhei.ttf', 46)
        font_name = ImageFont.truetype('fonts\\simhei.ttf', 46)
        font_class = ImageFont.truetype('fonts\\simhei.ttf', 20)
        draw.text((80, 30),content, (50, 51, 51), font=font_title)
        nL=0
        for i in name:
            L=2
            if(str.isdigit(i)):
                L=1
            nL += L
        cL=0
        for i in cla:
            claL=2
            if(str.isdigit(i)):
                claL=1
            cL += claL

        if(type(name)==float):
            # namePlace = (self.w-len(str(name)))/2-6.5*len(str(name))
            name=str(name).split('.')[0]
        # elif(type(name)==str):
        namePlace = (self.w - nL)/2 - 11*nL

        nameVPlace = self.h - 85
        if(type(cla)==float):
        #     claPlace = (self.w-len(str(cla)))/2-6.5*len(str(cla))
            cla = str(cla).split('.')[0]
        # elif(type(cla)==str):
        claPlace = (self.w - cL)/2 - 4*cL
        claVPlace = self.h - 40
        draw.text((namePlace, nameVPlace),name, (50, 51, 51), font=font_name)
        draw.text((claPlace, claVPlace), cla, (50, 51, 51), font=font_class)
        img = img.convert("RGB")
        filename =self.outFile + file
        img.save(filename)

    def name(self, QR_IMG, txt, file):
        img = Image.open(QR_IMG)
        draw = ImageDraw.Draw(img)
        font = ImageFont.truetype(self.TTF, 24)
        draw.text((300, 660),txt, (50, 51, 51), font=font)
        img = img.convert("RGB")
        filename = self.outFile + file
        img.save(filename)

    def capID(self, QR_IMG, txt, file):

        img = Image.open(QR_IMG)
        draw = ImageDraw.Draw(img)
        font = ImageFont.truetype(self.TTF, 24)
        draw.text((290, 710),txt, (50, 51, 51), font=font)
        img = img.convert("RGB")
        filename = self.outFile + file
        img.save(filename)

    def makePdf(self,pdfFileName, imageFiles):
        filePath = self.outFile + '/'
        cover = Image.open(filePath+imageFiles[0])
        width, height = cover.size
        pdf = FPDF(unit = 'pt', format = [width, height])
        print('Now creating PDF file, please wait...')
        for page in imageFiles:
            pdf.add_page()
            pdf.image(filePath+page, 0,0)
        pdf.output(pdfFileName, "F")


    def mainPro(self):
        start=time.time()
        content='正在生成二维码，请稍候......'
        self.output_to_console(f"{content}\n")
        filePath = self.outFile + '/'
        df = pd.read_csv(self.filename, sep=',', encoding='utf-8')

        df=df.fillna('')

        for i in df.index:
            content=df.loc[i,'id']
            content = "https://www.longstyle.cn/QDGM/qrcode?id=" + str(content)
            Title="青岛工贸职业学校资产管理系统"
            Name = df.loc[i,'name']
            id=str(df.loc[i, 'id'])
            capital_class=str(df.loc[i,'capital_class'])
            CLASS=str(df.loc[i,'class'])
            loc=str(df.loc[i,'location'])
            cla=capital_class+'_'+CLASS + '_' + loc

            if(cla==False):
                cla = id
            if(type(cla)==float):
                cla = float(str(cla).split('.')[0])
            if(Name==''):
                Name=df.loc[i, 'name']
            if(type(Name)==float):

                Name = str(Name).split('.')[0]


            if(cla):
                filename = str(Name) +'_'+ cla + '_' + id + '.png'
            else:
                filename = str(Name) + '_' + id + '.png'

            self.make_qr(content,filename)
            self.codeTitle(self.orimg_path +'ori_'+filename, Title, Name, cla, filename)
            # self.name(self.outFile +filename, Name, filename)
            # self.capID(self.outFile +filename, Location, filename )
            print('%s completed, continue...' % filename)

        os.makedirs(filePath+'pdf', exist_ok=True)
        files = os.listdir(filePath)
        fileList = []
        for file in files:
            if '.png' in file:
                fileList.append(file)

        T=datetime.datetime.now()
        if(len(str(T.minute))==1):
            Minute = '0'+str(T.minute)
        else:
            Minute = str(T.minute)
        DT = str(T.year)+str(T.month)+str(T.day)+str(T.hour)+Minute
        self.makePdf(filePath+'pdf/' + DT + '_qrCode.pdf', fileList )
        shutil.rmtree('orimg')
        end=time.time()
        dur = end - start
        content='创建完成。共生成 %s个文件。耗时：%s秒。结果保存在：%s中' % (i+1, dur,self.outFile)
        self.output_to_console(f"{content}\n")
        print('创建完成。共生成 %s个文件。耗时：%s秒。结果保存在：%s中' % (i+1, dur,self.outFile))



if __name__ == '__main__':
    app = App()
    app.resizable(height=False)
    app.mainloop()
